Discord = require('discord.js');
exports.run = async (client, msg) => {
  if (!client.user.bot) return msg.reply("Why would you need an invite link for a selfbot...");
  let embed = new Discord.RichEmbed().setTitle('Invite link').setDescription(`You can add ${client.user.username} by clicking [this link](https://discordapp.com/oauth2/authorize?client_id=510744698038648832&scope=bot&permissions=116736)`).setColor('#00ff00');
  msg.react("✔");
  return msg.author.send(embed)
};

exports.conf = {
  enabled: true,
  runIn: ["text", "dm"],
  aliases: [],
  permLevel: 0,
  botPerms: ["SEND_MESSAGES"],
  requiredFuncs: [],
  requiredSettings: [],
};

exports.help = {
  name: "invite",
  description: "Displays the join server link of the bot.",
  usage: "",
  usageDelim: "",
};
