Discord = require('discord.js');
exports.run = async (client, msg) => {
    let information =new Discord.RichEmbed().setTitle("Info").setColor("#00ff00").setDescription(`${client.user.username} is created by Fedox. It's highly unrecomended using nsfw commands unless you are 18yo+`)
     msg.send(information);
};

exports.conf = {
    enabled: true,
    runIn: ["text", "dm", "group"],
    aliases: ["details", "what"],
    permLevel: 0,
    botPerms: ["SEND_MESSAGES"],
    requiredFuncs: [],
    requiredSettings: [],
};

exports.help = {
    name: "info",
    description: "Provides some information about this bot.",
    usage: "",
    usageDelim: "",
};
