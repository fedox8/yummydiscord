const cli = require('nekos.life');
const neko = new cli();
exports.run = async (client, msg) => {
    msg.channel.startTyping();
    url = await neko.getSFWMeow();
    await msg.channel.send({
        files: [url.url]
    });
    msg.channel.stopTyping();
};

exports.conf = {
    enabled: true,
    runIn: ['text', 'dm'],
    aliases: ['sfwcat', 'cat'],
    permLevel: 0,
    botPerms: [],
    requiredFuncs: [],
    cooldown: 0
};

exports.help = {
    name: 'sfwCat',
    description: 'Gets a cat gif/image',
    usage: '',
    usageDelim: '',
    extendedHelp: ''
};
