const cli = require('nekos.life');
const neko = new cli();
exports.run = async (client, msg) => {
    msg.channel.startTyping();
    url = await neko.getSFWSlap();
    await msg.channel.send({
        files: [url.url]
    });
    msg.channel.stopTyping();
};

exports.conf = {
    enabled: true,
    runIn: ['text', 'dm'],
    aliases: ['sfwslap', 'slap'],
    permLevel: 0,
    botPerms: [],
    requiredFuncs: [],
    cooldown: 0
};

exports.help = {
    name: 'sfwSlap',
    description: 'Gets a slap gif/image',
    usage: '',
    usageDelim: '',
    extendedHelp: ''
};
