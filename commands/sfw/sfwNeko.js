const cli = require('nekos.life');
const neko = new cli();
exports.run = async (client, msg) => {
    msg.channel.startTyping();
    url = await neko.getSFWNeko();
    await msg.channel.send({
        files: [url.url]
    });
    msg.channel.stopTyping();
};

exports.conf = {
    enabled: true,
    runIn: ['text', 'dm'],
    aliases: ['sfwneko', 'neko'],
    permLevel: 0,
    botPerms: [],
    requiredFuncs: [],
    cooldown: 0
};

exports.help = {
    name: 'sfwNeko',
    description: 'Gets a neko image',
    usage: '',
    usageDelim: '',
    extendedHelp: ''
};
