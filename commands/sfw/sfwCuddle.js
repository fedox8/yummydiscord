const cli = require('nekos.life');
const neko = new cli();
exports.run = async (client, msg) => {
    msg.channel.startTyping();
    url = await neko.getSFWCuddle();
    await msg.channel.send({
        files: [url.url]
    });
    msg.channel.stopTyping();
};

exports.conf = {
    enabled: true,
    runIn: ['text', 'dm'],
    aliases: ['sfwcuddle', 'cuddle'],
    permLevel: 0,
    botPerms: [],
    requiredFuncs: [],
    cooldown: 0
};

exports.help = {
    name: 'sfwCuddle',
    description: 'Gets a cuddling gif/image',
    usage: '',
    usageDelim: '',
    extendedHelp: ''
};
