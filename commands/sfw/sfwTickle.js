const cli = require('nekos.life');
const neko = new cli();

exports.run = async (client, msg) => {
    msg.channel.startTyping();
    url = await neko.getSFWTickle();
    await msg.channel.send({
        files: [url.url]
    });
    msg.channel.stopTyping();
};

exports.conf = {
    enabled: true,
    runIn: ['text', 'dm'],
    aliases: ['sfwtickle', 'tickle'],
    permLevel: 0,
    botPerms: [],
    requiredFuncs: [],
    cooldown: 0
};

exports.help = {
    name: 'sfwTickle',
    description: 'Gets an tickle gif/image',
    usage: '',
    usageDelim: '',
    extendedHelp: ''
};
