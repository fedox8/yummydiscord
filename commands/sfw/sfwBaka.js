const cli = require('nekos.life');
const neko = new cli();
exports.run = async (client, message) => {
    message.channel.startTyping();
    url = await neko.getSFWBaka();
    await message.channel.send({
        files: [url.url]
    });
    message.channel.stopTyping();
};

exports.conf = {
    enabled: true,
    runIn: ['text', 'dm'],
    aliases: ['sfwbaka', 'baka'],
    permLevel: 0,
    botPerms: [],
    requiredFuncs: [],
    cooldown: 0
};

exports.help = {
    name: 'sfwBaka',
    description: 'Gets a baka gif/image',
    usage: '',
    usageDelim: '',
    extendedHelp: ''
};
