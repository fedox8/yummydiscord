const cli = require('nekos.life');
const neko = new cli();
exports.run = async (client, msg) => {
    msg.channel.startTyping();
    url = await neko.getSFWLizard();
    await msg.channel.send({
        files: [url.url]
    });
    msg.channel.stopTyping();
};

exports.conf = {
    enabled: true,
    runIn: ['text', 'dm'],
    aliases: ['sfwlizard', 'lizard'],
    permLevel: 0,
    botPerms: [],
    requiredFuncs: [],
    cooldown: 0
};

exports.help = {
    name: 'sfwLizard',
    description: 'Gets a lizard gif/image',
    usage: '',
    usageDelim: '',
    extendedHelp: ''
};
