const cli = require('nekos.life');
const neko = new cli();
exports.run = async (client, msg) => {
    if (msg.channel.type == 'text' && !msg.channel.nsfw === true) {
        msg.channel.send("You have to run this command in nsfw channel or in DM")
    } else {
        msg.channel.startTyping();
        url = await neko.getNSFWFeetGif();
        await msg.channel.send({
            files: [url.url]
        });
        msg.channel.stopTyping();
    }
};

exports.conf = {
    enabled: true,
    runIn: ['text', 'dm'],
    aliases: ['nsfwfeetg', 'feetg'],
    permLevel: 0,
    botPerms: [],
    requiredFuncs: [],
    cooldown: 0
};

exports.help = {
    name: 'nsfwFeetg',
    description: 'Gets a feet gif',
    usage: '',
    usageDelim: '',
    extendedHelp: ''
};
