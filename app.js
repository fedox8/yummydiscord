const Komada = require('./komada');
const config = require('./config');
const neko = require('nekos.life');

const client = new Komada.Client(config.botSettings);

client.login(config.botToken);
